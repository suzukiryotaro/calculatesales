package jp.alhinc.suzuki_ryotaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {
		Map<String, String> map1 = new HashMap<>(); //map1を宣言
		Map<String, Long> map2 = new HashMap<>(); //map2を宣言
		BufferedReader br = null;

		String lines[] = null;
		try {
			File file = new File(args[0], "branch.lst");

			//エラー処理（支店定義ファイルの存在チェック）
			if (!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {

				//カンマ区切り
				lines = line.split(",", -1);

				//エラー処理（支店定義ファイルが","の前後で2つになることチェック）
				if (lines.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//支店コード(lines[0])
				//支店名(lines[1])
				map1.put(lines[0], lines[1]); //map1に支店コード、支店名を入れる
				map2.put(lines[0], 0L);

				//支店定義ファイルが3桁の数字で始まる＆改行含まないチェック
				if (!(lines[0].matches("^[0-9]{3}") && lines[1].matches(".*$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//支店定義ファイルのキーをArrayListに格納
		List<String> shopId = new ArrayList<>(map1.keySet());

		try {
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {

					//指定文字列でフィルタする（0-9の中で8桁かつ末尾が.rcd）
					if (str.matches("^[0-9]{8}\\.rcd$")) {
						return true;
					} else {
						return false;
					}
				}
			};

			//listFilesメソッドを使用して一覧を取得する
			File[] list = new File(args[0]).listFiles(filter);

			//売上ファイルの連番チェック
			List<Integer> number = new ArrayList<>();
			for (int i = 0; i < list.length; i++) {
				String fileName = list[i].getName();
				number.add(Integer.parseInt(fileName.substring(0, 8)));
			}
			Collections.sort(number);

			for (int k = 1; k < number.size(); k++) {
				if (number.get(k) != number.get(k - 1) + 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr = new FileReader(list[i]);
					br = new BufferedReader(fr);
					String name = list[i].getName();

					//１行毎に読み込む
					String sale;//支店コード
					sale = br.readLine();
					String sale2;//支店別売上金額
					sale2 = br.readLine();

					//売上ファイルのフォーマットチェック
					String sale3;
					sale3 = br.readLine();
					if (sale3 != null) {
						System.out.println(name + "のフォーマットが不正です");
						return;
					}

					//支店コードがあってるかチェック（containsKeyメソッドを使えばもっと簡単に）
					if (!(shopId.contains(sale))) {
						System.out.println(name + "の支店コードが不正です");
						return;
					}

					//String型からlong型に変換
					long num = Long.parseLong(sale2);

					//売上額を取り出して合計
					long a = map2.get(sale);
					long b = num + a;

					//エラー処理（合計金額が10桁を超えたかチェック）
					if (b > 9999999999L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					//map2に支店コード、売上を入れる
					map2.put(sale, b);
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}

			try {
				//出力先を作成
				FileWriter fw = new FileWriter(new File(args[0], "branch.out"));
				PrintWriter pw = new PrintWriter(new BufferedWriter(fw));

				//内容を指定する(支店コード、支店名、合計金額を順番に表示かつ改行)
				for (Entry<String, String> entry : map1.entrySet()) {
					pw.println(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey()));
				}

				//ファイルに書き出す
				pw.close();

			} catch (IOException e) {
				//例外処理
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}
	}
}